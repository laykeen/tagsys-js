class TagSysHistory {
    /*
    "history"
        action           "Create, Update, Delete"
        target           "tag name"
        from             { old props }
        to               { new props }
    */
    _LIMIT = 100; // limit of history list length

    history = []; // list of ordered actions
    head = null; // current history position

    constructor(args={}) {
        // parse args and assign directly to this class
        for (var arg in args) {
            if (args.hasOwnProperty(arg)) {
                this[arg] = args[arg];
            }
        }
    }

    // TODO: add undoable C_UD actions
    pushHistory() {}
    // TODO: add redoable C_UD actions
    popHistory() {}
}
