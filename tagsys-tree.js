class TagSysTree {
    _DEBUG = false; // show info prints on the console
    _DEBUG_MSG_PREFIX = ":: tree::"; // prepend this string on every debug message

    /*
    node
        parent
        tags
        props
        system
        type
        state
            default
            current
            value
            last
            lastTimestamp
    */
    nodes = {};

    constructor(args={}) {
        // parse args and assign directly to this class
        for (var arg in args) {
            if (args.hasOwnProperty(arg)) {
                this[arg] = args[arg];
            }
        }
    }

    createNode() {}
    readNode() {}
    updateNode() {}
    deleteNode() {}

    /**
     * Console debugging in a "formatted" manner (easy to search in the logs)
     * @param {any} args - OPTIONAL - spread arguments to pass to console.log
     */
    _debug(...args) {
        if (this._DEBUG === true) {
            console.log(this._DEBUG_MSG_PREFIX, ...args);
        }
    }
}



class Node {

    tags = [];
    props = {};
    system = {};
    type = {};
    state = {};

    constructor() {

    }
}
