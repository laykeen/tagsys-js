class TagSysGraph {
    _DEBUG = false; // show info prints on the console
    _DEBUG_MSG_PREFIX = ":: graph::"; // prepend this string on every debug message

    /*
    nodes
        id
            tags
            props
            system
            type
            state
                current
                value
                default
                last
                last_timestamp
    edges
        id
            fromId
            toId
            tags
            props
            system
            type
            state
                current
                value
                default
                last
                last_timestamp
    */

    nodes = {}; // graph nodes map
    edges = {}; // graph edges map

    constructor(args={}) {
        // parse args and assign directly to this class
        for (var arg in args) {
            if (args.hasOwnProperty(arg)) {
                this[arg] = args[arg];
            }
        }
    }

    /**
     * Console debugging in a "formatted" manner (easy to search in the logs)
     * @param {any} args - OPTIONAL - spread arguments to pass to console.log
     */
    _debug(...args) {
        if (this._DEBUG === true) {
            console.log(this._DEBUG_MSG_PREFIX, ...args);
        }
    }
}





/*
"project"
    nodes
        "ulid"           "timestamp + UUID"
            tags         [ tag, tag, tag ]
            states       { current, last, initial props }
            props        { ... }
            sys props    { node props }
            type         "class name"
    edges
        "ulid"           "timestamp + UUID"
            tags         [ tag, tag, tag ]
            from         "node ulid"
            to           "node ulid"
            states       { current, last, initial props }
            props        { ... }
            sys props    { edge props }
            type         "class name"
*/
