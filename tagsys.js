class TagSys {
    _DEBUG = false; // show info prints on the console
    _DEBUG_MSG_PREFIX = ":: tagsys::"; // prepend this string on every debug message
    _NAMESPACER = ":"; // string used to separate a namespace-level tag from a tag, both in the tags_map and in the tags_queue (also in schema in the "inherit" list)
    _SEPARATOR = ","; // string used to separate tags in the "inherit" field
    _TYPE_REQUIRED = false; // map only tags with the "type" field
    _NAMESPACE_TAG = false; // if a tag has "children" field but can be saved as tag (_TYPE_REQUIRED ignore this)
    _OVERRIDE_DUPLICATES = true; // on importing a duplicate tag, keep the old one or override it with the new tag
    _ALSO_DELETE_TAG_QUEUE = true; // on remove also clear that tag as a completed dependency
    _VERSIONING = true; // update system.version on Create and Update operations

    schema = {}; // current tags schema to regenerate / export it later
    tags_map = {}; // here were tags
    tags_queue = {}; // a map of incomplete tags and an ordered list of complete/uncomplete tags (when all are completed, they are merged in order to follow tag inheritance)

    // possible tag system callbacks if needed
    // onInit = (map, queue) => {
    //     this._debug("onInit", map, queue);
    // };
    // onSetSchema = (schema) => {
    //     this._debug("onSetSchema", schema);
    // };
    // onSetSchemaError = (err) => {
    //     this._debug("onSetSchemaError", err);
    // };
    // onMergeSchema = (map) => {
    //     this._debug("onMergeSchema", map);
    // };
    // onMergeSchemaError = (err) => {
    //     this._debug("onMergeSchemaError", err);
    // };
    // onAddTag = (tag) => {
    //     this._debug("onAddTag", tag);
    // };
    // onAddTagError = (err) => {
    //     this._debug("onAddTagError", err);
    // };
    // onPushQueue = (list) => {
    //     this._debug("onPushQueue", list);
    // };
    // onPopQueue = (tag) => {
    //     this._debug("onPopQueue", tag);
    // };
    // onCreateTag = (name, schema) => {
    //     this._debug("onCreateTag", name, schema);
    // };
    // onUpdateTag = (name, tag) => {
    //     this._debug("onUpdateTag", name, tag);
    // };
    // onDeleteTag = (name) => {
    //     this._debug("onDeleteTag", name);
    // }
    // onDeleteOnQueue = (name) => {
    //     this._debug("onDeleteOnQueue", name);
    // }
    // onDeleteOnQueueDependency = (name) => {
    //     this._debug("onDeleteOnQueueDependency", name);
    // }

    /**
     * Istantiate the tag system with some defaults
     * @constructor
     * @param {Object} args - OPTIONAL - used to customize how the tag system
     *  generate the tag map and other customization parameters
     * @return {TagSys} - this object
     */
    constructor(args={}) {
        // parse args and assign directly to this class
        for (var arg in args) {
            if (args.hasOwnProperty(arg)) {
                this[arg] = args[arg];
            }
        }
    }

    /**
     * Parse a schema
     * @constructor
     * @param {Object} schema - REQUIRED - accept a json structured like:
     *     "namespace"
     *       "inherit"        [ "tag", "tag", "tag", ... ]
     *       "system"         { "children types", ... }
     *         "version"      0 (incremented the minor on every update)
     *       "props"          { ... }
     *       "type"           "class name"
     *       "children"
     *          "tag"
     *             "inherit"  [ "tag", "tag", "tag", ... ]
     *             "system"   { "constructor props", ... }
     *             "props"    { ... }
     *             "type"     "class name"
     * @return {Promise} - fulfill when everything in the schema is parsed
     */
    init(schema={}) {
        // reset this tagsys state
        this.tags_map = {};
        this.schema = {};
        this.tags_queue = {};

        this._debug("init:", schema);

        // both parsing and cloning of schema can be made async
        return Promise.all([
            this.setSchema(schema),
            this.mergeSchema(schema)
        ]).then(() => {
            // completed tag system load
            this._debug("complete:", this);
            if (this.onInit !== undefined) {
                new Promise(() => this.onInit(this.tags_map, this.tags_queue));
            }
        }).catch(
            (...error) => this._debug(...error)
        );
    }

    /**
     * Export the current state of this object in an ordered manner
     * @param {boolean} config - OPTIONAL - flag to also export current tag system setup
     * @return {Promise} on fulfill return a map of tags, schema and uncomplete tags, also configs if explicitely requested
     */
    get(configs=false) {
        let json = {};
        return new Promise((resolve, reject) => {
            if (configs === true) {
                this.getConfig().then(resolve)
            } else {
                resolve(json);
            }
        })
        .then((json) => this.getMap(json))
        .then((json) => this.getQueue(json))
        .then((json) => this.getSchema(json))
        .catch((...error) => this._debug(...error));
    }

    /**
     * Export the tag system setup
     * @param {Object} json - OPTIONAL - partial exports json
     * @return {Promise} the json param itself with the configs
     */
    getConfig(json={}) {
        return new Promise((resolve, reject) => {
            try {
                json.settings = {};
                try {
                    for (var config in this) {
                        if (!this.hasOwnProperty(config)) {
                            continue;
                        }
                        // every "private" property of this class is a config
                        if (config.charAt(0) === "_") {
                            if (typeof this[config] === "object") {
                                json.settings[config] = JSON.stringify(
                                    JSON.parse(this[config])
                                );
                            } else {
                                json.settings[config] = this[config];
                            }
                        }
                    }
                } catch (err) {
                    console.error(err);
                    reject("error on export TagSys configs", err);
                }
                resolve(json);
            } catch (err) {
                reject("error on getConfig", err);
            }
        });
    }

    /**
     * Export the tags' map into the json parameter
     * @param {Object} json - OPTIONAL - partial exports json
     * @return {Promise} the json param itself with the map
     */
    getMap(json={}) {
        // TODO: implement a filtered export
        return new Promise((resolve, reject) => {
            try {
                json.tags_map = JSON.parse(JSON.stringify(this.tags_map));
                resolve(json);
            } catch (err) {
                reject("error on getMap");
            }
        });
    }

    /**
     * Export the map of queued tags into the json parameter
     * @param {Object} json - OPTIONAL - partial exports json
     * @return {Promise} the json param itself with the queue
     */
    getQueue(json={}) {
        return new Promise((resolve, reject) => {
            try {
                let tags = {};
                for (let name in this.tags_queue) {
                    if (this.tags_queue.hasOwnProperty(name)) {
                        let tag = this.tags_queue[name];
                        let deps = [];
                        for (var dep of tag) {
                            // save only real dependencies
                            if (Object.keys(dep.tag).length === 0 &&
                                dep.dependency !== undefined) {
                                deps.push(dep.dependency);
                            }
                        }
                        tags[name] = deps;
                    }
                }
                json.tags_queue = tags;
                resolve(json);
            } catch (err) {
                reject("error on getQueue");
            }
        });
    }

    /**
     * Export the current tag system schema into the json parameter
     * @param {Object} json - OPTIONAL - partial exports json
     * @return {Promise} the json param itself with the schema
     */
    getSchema(json={}) {
        return new Promise((resolve, reject) => {
            try {
                json.schema = this.lintSchema(this.schema);
                resolve(json);
            } catch (err) {
                console.error(err);
                reject("error on getSchema");
            }
        });
    }

    /**
     * remove useless parameters from the schema as a tag sys schema linter
     * @param {Object} root - REQUIRED - schema root to start parsing (usually a namespace)
     * @return {Object} the linted schema
     */
    lintSchema(root={}) {
        let schema = {};
        for (let namespace in root) {
            if (root.hasOwnProperty(namespace)) {
                const ns = root[namespace];
                let tag = {};
                // validate every single type of tag parameter
                if (ns.hasOwnProperty("inherit")) {
                    let inherit = this._splitInherit(ns.inherit);
                    if (inherit.length > 0) {
                        tag.inherit = JSON.parse(JSON.stringify(
                            inherit
                        ));
                    }
                }
                if (ns.hasOwnProperty("system")) {
                    if (Object.keys(ns.system).length > 0) {
                        tag.system = JSON.parse(JSON.stringify(
                            ns.system
                        ));
                    }
                }
                if (ns.hasOwnProperty("type")) {
                    tag.type = JSON.parse(JSON.stringify(
                        ns.type
                    ));
                }
                if (ns.hasOwnProperty("props")) {
                    if (Object.keys(ns.props).length > 0) {
                        tag.props = JSON.parse(JSON.stringify(
                            ns.props
                        ));
                    }
                }
                // recursively lint children tags
                if (ns.hasOwnProperty("children")) {
                    tag.children = this.lintSchema(ns.children);
                }
                schema[namespace] = tag;
            }
        }
        return schema;
    }

    /**
     * Promisified schema clone for local references
     * @param {Object} schema - REQUIRED - schema to clone into this class
     * @return {Promise} fulfill after json parse
     */
    setSchema(schema) {
        return new Promise((resolve, reject) => {
            try {
                // deep clone of schema object
                this.schema = JSON.parse(JSON.stringify(schema));
                if (this.onSetSchema !== undefined) {
                    new Promise(() => this.onSetSchema(this.schema));
                }
            } catch (error) {
                console.error("setSchema error", error);
                if (this.onSetSchemaError !== undefined) {
                    new Promise(() => this.onSetSchemaError(error));
                }
                reject(error);
            }
            resolve();
        });
    }

    /**
     * edit in runtime some callbacks or settings
     * @param {string} prop - REQUIRED - property name to change
     * @param {any} value - REQUIRED - value to set the property (keeping references (NO DEEP OBJECT CLONING!))
     * @return {boolean} if the prop exists in this object
     */
    set(prop="", value) {
        if (this.hasOwnProperty(prop)) {
            this[prop] = value;
            return true;
        }
        return false;
    }



    /**
     * translate a tag system schema to a map of tags and a queue of incomplete tags under a promise
     * @param {Object} schema - REQUIRED - json of tags to parse and merge with the current tags map
     * @return {Promise} merge schema error on reject, tags map on success
     */
    mergeSchema(schema, map=this.tags_map, queue=this.tags_queue) {
        return new Promise((resolve, reject) => {
            try {
                this._parseSchema(schema, {}, map, queue);
                if (this.onMergeSchema !== undefined) {
                    new Promise(() => this.onMergeSchema(this.tags_map));
                }
                resolve(this.tags_map);
            } catch (error) {
                console.error("mergeSchema error", error);
                if (this.onMergeSchemaError !== undefined) {
                    new Promise(() => this.onMergeSchemaError(error));
                }
                reject("merge schema error:", error);
            }
        });
    }

    /**
     * translate a tag system schema to a tag map and a queue of incomplete tags
     * @param {Object} schema - OPTIONAL - json of tags to parse
     * @param {Object} parent - OPTIONAL - namespace parent to use for name and props inheritance
     * @param {Map} map - OPTIONAL - tag system tags map
     * @param {Map} queue - OPTIONAL - tag system tags queue
     * @return undefined
     */
    _parseSchema(schema={}, parent={}, map=this.tags_map, queue=this.tags_queue) {
        // loop the "namespace" level of tags
        for (let namespace in schema) {
            if (!schema.hasOwnProperty(namespace)) {
                continue;
            }
            const ns = schema[namespace];

            // extract "inherit" tags and use them to complete the current tag
            let missing = [];
            if (ns.inherit !== undefined) {
                if (ns.inherit.length > 0) {
                    let extract = this._inherit(
                        this._splitInherit(ns.inherit, this._SEPARATOR), map
                    );
                    // keep missed tags list
                    if (extract.miss.length > 0) {
                        this._debug("tag `" +
                            (parent.name ?
                                (parent.name + this._NAMESPACER + namespace) :
                                namespace
                            ) +
                            "` missed tags to inherit:",
                            extract.miss.join(","),
                            "\n\t\t\t current saved tags:",
                            Object.keys(map).join(",")
                        );
                        missing = extract.miss;
                    }
                    /* join inherited tags into the current parent tag,
                     * following the "namespace -> inherit -> tag" order
                     * also deep clone every object to avoid
                     * any kind of cross references,
                     * and keep only the current tag name as is
                     */
                    parent = Object.assign({},
                        JSON.parse(JSON.stringify(parent)),
                        JSON.parse(JSON.stringify(extract.inherited)),
                        { name: parent.name }
                    );
                }
            }

            // merge parent tag and inherited tags data into current tag
            let tag = this._parseTag(namespace, ns, parent);

            // with at least 1 missed tag to import, do not create the current tag and put it in a queue with all missing and partial imported tags
            if (missing.length > 0) {
                queue[tag.name] = this._pushQueue(tag, missing, map);
            } else {

                // the tag is complete, check for typed tags
                if (tag.type !== undefined || !this._TYPE_REQUIRED) {
                    let created = undefined;
                    // check also for namespaces instead of tags, and create it only if explicitely requested
                    if (ns.children !== undefined) {
                        if (this._NAMESPACE_TAG) {
                            created = this._addTag(tag, map);
                        }
                    } else {
                        created = this._addTag(tag, map);
                    }

                    // after a tag is completed, check for queued tags waiting for it as a dependency
                    if (created !== undefined) {
                        this._popQueue(created, queue);
                    }
                }

                // recursively parse children tags and use this tag, now namespace, as parent
                if (ns.children !== undefined) {
                    this._parseSchema(ns.children, tag, map, queue);
                }

            }
        }
    }

    /**
     * validate and assign the newly created tag to the tag system map
     * @param {Object} json - REQUIRED - tag json to map
     * @param {Object} map - REQUIRED - target map
     * @return {Object} the mapped tag
     */
    _addTag(json={}, map=this.tags_map) {
        let tag = {};

        // check for void json
        if (Object.keys(json).length === 0) {
            return undefined;
        }
        try {
            // validate name
            let name = JSON.parse(JSON.stringify(json.name));
            if (name.includes(this._SEPARATOR)) {
                this._debug("invalid tag name found! `" + name + "` contains the tags separator `" + this._SEPARATOR + "`!");
                return undefined;
            }
            // check for duplicates
            if (map[name] !== undefined) {
                this._debug("duplicate tag found `" + name + "`", map[name], json);
                if (this._OVERRIDE_DUPLICATES) {
                    this._debug("overriding tag `" + name + "`...");
                    delete map[name];
                }
            }
            // and finally map the tag
            tag = JSON.parse(JSON.stringify(json));
            map[name] = tag;
        } catch (error) {
            this._debug("error on mapping tag:", error);
            if (this.onAddTagError !== undefined) {
                new Promise(() => this.onAddTagError(error));
            }
            return undefined;
        }

        if (this.onAddTag !== undefined) {
            new Promise(() => this.onAddTag(tag));
        }

        return tag;
    }

    /**
     * utility function to merge all inherited props, parent props and current tag props into a single tag json
     * @param {string} name - REQUIRED - complete (with all namespaces and separators) tag name
     * @param {Object} schema - REQUIRED - current parsed tag props
     * @param {Object} parent - OPTIONAL - inherited and parent props
     * @param {string} namespacer - OPTIONAL - string to separate namespace name from tag name
     * @return {Object} newly created tag ready to be mapped and used
     */
    _parseTag(name="", schema={}, parent={}, namespacer=this._NAMESPACER) {
        let tag = {
            name: name,
            system: {},
            props: {},
            type: schema.type
        };

        // deep clone every object to avoid references as usual
        tag.system = Object.assign({},
            JSON.parse(JSON.stringify(parent.system ?? {})),
            JSON.parse(JSON.stringify(schema.system ?? {}))
        );
        // keep track of tag version inside system.version prop
        if (this._VERSIONING === true && tag.system.version === undefined) {
            tag.system.version = 1;
        }
        tag.props = Object.assign({},
            JSON.parse(JSON.stringify(parent.props ?? {})),
            JSON.parse(JSON.stringify(schema.props ?? {}))
        );
        // final merge for tag name (parent name can be a multi-level namespace)
        if (parent.name) {
            tag.name = parent.name + namespacer + tag.name;
        }

        return tag;
    }

    /**
     * Validate "inherit" parameter from string to array if required
     * @param {Array | string} tags - OPTIONAL - list of tags
     * @param {string} separator - OPTIONAL - string to separate two tags (fallback to "," in philomena style)
     * @return {Array} converted string to array of tags to inherit
     */
    _splitInherit(tags=[], separator=this._SEPARATOR) {
        let filter = [];

        // convert only string to array
        if (typeof tags === "string") {
            let split = tags.split(separator);
            for (let i = 0, slen = split.length; i < slen; i++) {
                // remove whitespaces from splitted tags
                // NO TAGS SHOULD START OR END WITH SPACES, THEY WILL BE TRIMMED
                let cleared = split[i].trim();
                if (cleared.length) {
                    filter.push(cleared);
                }
            }
        } else {
            // already an array of tags to import
            filter = tags;
        }

        return filter;
    }

    /**
     * Parse a list of tags to extract in the exact order every tag data and merge it into a single "inherited" object.
     * For every missed tag, a "miss" structure is created
     * @param {Array} tags - REQUIRED - ordered list of tags to inherit
     * @param {Map} map - OPTIONAL - tag system tags map
     * @return {Object} return an object as { miss, inherited }
     */
    _inherit(tags=[], map=this.tags_map) {
        let extract = {
            miss: [],
            inherited: {}
        };

        for (let tag of tags) {
            /* on a tag miss, every subsequent tag is missed because the inheritance follow a strict order.
             * an example: [t1, t2, t3] -> t2 missing -> inherited: t1, miss: t2, t3
             * when t2 is completed, re-run inherit steps until every tag is completed (miss length is 0)
             */
            if (map[tag] === undefined || extract.miss.length > 0) {
                extract.miss.push(tag);
            } else {
                // shallow merge of tags (and a complete copy of the selected tag to avoid object references)
                extract.inherited = Object.assign(
                    extract.inherited,
                    JSON.parse(JSON.stringify(map[tag]))
                );
            }
        }

        return extract;
    }

    /**
     * save a tag with a missing list of tags to import
     * @param {Object} tag - REQUIRED - tag with some incomplete inherits
     * @param {Array} misses - REQUIRED - list of imported / missed tags
     * @param {Map} map - OPTIONAL - tag system tags map
     * @return {Object} the sorted and filled tag queue
     */
    _pushQueue(tag={}, misses=[], map=this.tags_map) {
        let list = [];

        for (let i = 0, len = misses.length; i < len; i++) {
            // every inherit tag is saved
            let dep = {
                dependency: misses[i],
                tag: {}
            };
            // save completed inherit tags into the queue to know when a queued tag is complete and to merge the inherits
            if (map[misses[i]] !== undefined) {
                dep.tag = map[misses[i]];
            }
            list.push(dep);
        }
        // the last dependency is the tag itself (highest inheritance priority)
        list.push({
            tag: tag
        });

        if (this.onPushQueue !== undefined) {
            new Promise(() => this.onPushQueue(list));
        }

        return list;
    }

    /**
     * when a tag is complete, search in the incomplete tags if someone needs it
     * @param {Object} tag - REQUIRED - the completed tag
     * @param {Map} queue - OPTIONAL - the tags queue
     * @return {bool} if someone had this tag as a dependency
     */
    _popQueue(tag={}, queue=this.tags_queue) {
        let needed = false;

        // scan the entire queue
        for (let dep in queue) {
            if (!queue.hasOwnProperty(dep)) {
                continue;
            }
            let item = queue[dep];
            let len = item.length;
            let complete = true;
            // check if the current queued tag can be completed
            for (let i = 0; i < len; i++) {
                // if this tag depends on the currently completed tag save a copy of it
                if (item[i].dependency === tag.name) {
                    item[i].tag = JSON.parse(JSON.stringify(tag));
                    needed = true;
                }
                // instead when a single dependency is missing, the queued tag cannot be completed
                if (complete && item[i].tag.name === undefined) {
                    complete = false;
                }
            }
            if (complete) {
                // the original tag is always the last one
                let new_tag = item[len - 1].tag;
                // merge the other dependencies data
                for (var i = 0; i < len - 1; i++) {
                    let part = item[i].tag;
                    Object.assign(new_tag.props, part.props);
                    Object.assign(new_tag.system, part.system);
                }
                // free the completed tag from the queue
                delete queue[dep];

                if (this.onPopQueue !== undefined) {
                    new Promise(() => this.onPopQueue(dep));
                }

                this._addTag(new_tag);
                this._debug("tag `" + new_tag.name + "` completed");
            }
        }

        return needed;
    }

    /**
     * Console debugging in a "formatted" manner (easy to search in the logs)
     * @param {any} args - OPTIONAL - spread arguments to pass to console.log
     */
    _debug(...args) {
        if (this._DEBUG === true) {
            console.log(this._DEBUG_MSG_PREFIX, ...args);
        }
    }

    /**
     * convert a namespace:tag tag name into a schema
     * example: namespace:tag create a namespace: { children: { tag: {} }} schema structure
     * @param {Array} name - REQUIRED - tag namespaces and name already splitted
     * @param {Array} inherit - OPTIONAL - inherit prop to apply to the tag
     * @param {Object} data - OPTIONAL - props and system of tag
     * @return {Object} schema derived from name
     */
    _nameToSchema(name=[], inherit=[], data={}) {
        let ns = name.shift();
        let schema = {};

        schema[ns] = {};
        // create the tag hierarchy for every namespace
        if (name.length > 0) {
            schema[ns].children = this._nameToSchema(name, inherit, data);
        } else {
            // and the tag data as the last name
            schema[ns].inherit = inherit;
            if (data.type !== undefined) {
                schema[ns].type = data.type;
            }
            if (data.system !== undefined) {
                schema[ns].system = JSON.parse(JSON.stringify(data.system));
            }
            if (data.props !== undefined) {
                schema[ns].props = JSON.parse(JSON.stringify(data.props));
            }
        }

        return schema;
    }



    /**
     * Create a new tag and add it to the map
     * @param {string} name - REQUIRED - the complete tag name (with namespaces, if they aren't saved inside the schema, they will be created)
     * @param {Array | string} inherit - OPTIONAL - list of tags to inherit
     * @param {Object} data - OPTIONAL - props and system data of the tag
     * @return {Promise} on fulfill return the newly created tag
     */
    create(name="", inherit=[], data={}) {
        if (this.tags_map[name] === undefined || this._OVERRIDE_DUPLICATES) {
            return this.update(name, inherit, data, true)
            .then((tag) => {
                if (this.onCreateTag !== undefined) {
                    new Promise(() => this.onCreateTag(name, tag));
                }
                return new Promise((resv, rej) => {
                    resv(tag);
                });
            });
        } else {
            return new Promise((resolve, reject) => {
                resolve(this.tags_map[name]);
            });
        }
    }

    /**
     * get the mapped tag
     * @param {string} name - REQUIRED - complete tag name (with separators)
     * @return {Object} the mapped tag if it exists
     */
    read(name="") {
        return new Promise((resolve, reject) => {
            if (!name.length) {
                reject(name.length);
            }
            resolve(this.tags_map[name]);
        });
    }

    /**
     * Update a tag in the map
     * @param {string} name - REQUIRED - the complete tag name (with namespaces, if they aren't saved inside the schema, they will be created)
     * @param {Array | string} inherit - OPTIONAL - list of tags to inherit
     * @param {Object} data - OPTIONAL - props and system data of the tag
     * @param {boolean} isCreate - OPTIONAL - check if update is called by create function (to avoid onUpdate call)
     * @return {Promise} on fulfill return the newly created tag, on missing inherit or on missing tag reject it
     */
    update(name="", inherit=[], data={}, isCreate=false) {
        return new Promise((resolve, reject) => {
            let schema = {};

            try {
                // a tag name is always required
                if (name.length === 0) {
                    throw name;
                }
                let namespaces = name.split(this._NAMESPACER);

                if (this._VERSIONING === true) {
                    // keep track of tag version inside system.version prop
                    if (this.tags_map[name] !== undefined) {
                        // keep specified version on the update
                        let version = this.tags_map[name].system.version + 1;
                        if (data.system !== undefined) {
                            if (data.system.version === undefined) {
                                data.system.version = version;
                            }
                        } else {
                            data.system = {
                                version: version
                            };
                        }
                    }
                }

                schema = this._nameToSchema(namespaces, inherit, data);

                // merge created schema into current tag map
                this.mergeSchema(schema)
                .then(() => {
                    if (this.onUpdateTag !== undefined && !isCreate) {
                        new Promise(() => this.onUpdateTag(name, schema));
                    }
                })
                .then(() => resolve(this.tags_map[name]))
                .catch(reject);
            } catch (error) {
                reject("error on update tag!", error);
            }
        });
    }

    /**
     * Remove a tag from the map
     * @param {string} name - REQUIRED - the complete tag name to remove
     * @return {Promise} on fulfill if the requested tag is removed, reject if it doesn't exists
     */
    delete(name="") {
        return new Promise((resolve, reject) => {
            if (this.tags_map[name] !== undefined) {
                if (this.onDeleteTag) {
                    this.onDeleteTag(name);
                }
                delete this.tags_map[name];
            } else if (this.tags_queue[name] !== undefined) {
                if (this.onDeleteOnQueue) {
                    this.onDeleteOnQueue(name);
                }
                delete this.tags_queue[name];
            } else {
                resolve(false);
            }
            // remove it as a dependency from every other tag in the queue
            if (this._ALSO_DELETE_TAG_QUEUE === true) {
                for (var dep in this.tags_queue) {
                    if (this.tags_queue.hasOwnProperty(dep)) {
                        let queued = this.tags_queue[dep];
                        for (let i = 0, len = queued.length; i < len; i++) {
                            if (queued[i].dependency === name) {
                                // remove the dependency tag
                                let removed = queued.splice(i, 1);
                                i--;
                                len--;
                                if (this.onDeleteOnQueueDependency) {
                                    this.onDeleteOnQueueDependency(removed);
                                }
                            }
                        }
                    }
                }
            }
            resolve(true);
        });
    }
}
